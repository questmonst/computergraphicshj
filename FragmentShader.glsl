#version 330 core

// 버텍스셰이더에서 이주된 값
in vec3 fragmentColor;

// 출력 데이터
out vec3 color;

void main(){

	// 출력 색상 = 버텍스셰이더에서 출력색상 결정
	// 3개의 버텍스 사이의 색으로 인터폴레이트됨
	color = fragmentColor;

}