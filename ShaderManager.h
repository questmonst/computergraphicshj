#pragma once
#include "Header.h"

class Triangle;

class ShaderManager
{
	const GLchar* VertexShaderSource;
	const GLchar* FragmentShaderSource;

public:
	//Ids
	GLuint VertexShader;
	GLuint FragmentShader;
	GLuint ShaderProgramID;
	GLuint MatrixID;

	//Default Rotation
	const GLfloat DefaultRotationX = 30.f;
	const GLfloat DefaultRotationY = -30.f;
	const GLfloat DefaultRotationZ = -30.f;
	const GLfloat DefalutAngle = 5.f;

	GLfloat RotationX;
	GLfloat RotationY;
	GLfloat RotationZ;

	GLfloat LocationX;
	GLfloat LocationY;
	GLfloat LocationZ;

	//Cameras - Not Used Yet
	GLfloat CameraX;
	GLfloat CameraZ;
	GLfloat CameraY;
	glm::vec3 cameraPos;
	glm::vec3 cameraDirection;
	glm::vec3 cameraUp;

	//Model Location(is it address?)
	int ModelLocation;
	int ViewLocation;
	int ProjectionLocation;

	//MVPs 
	glm::mat4 View;
	glm::mat4 Projection;
	glm::mat4 Model;
	glm::mat4 MVP;
	glm::mat4 TempModel;

	//Buffers
	GLuint VertexBuffer_Cube;
	GLuint ColorBuffer_Cube;

	GLuint VertexBuffer_Line;
	GLuint ColorBuffer_Line;

	GLuint VertexBuffer_Pyramid;
	GLuint ColorBuffer_Pyramid;

	//modes
	bool bUseDepth;
	RotationMode XMode;
	RotationMode YMode;
	bool bFillPolies;
	bool bUseCube;

private:
	//OldVAO,VBos
	GLuint VAO[5], VBO[10];
	Triangle* Triangles[4];
	const int VAOMax = 5;
	const int VBOMax = 10;
	const int TriMax = 4;

	//NewVAo,VBO -Not Used Yet
	GLuint UnderVAO;

	//No More Used
	GLfloat ColorsS[3][6] = {
		// �ﰢ�� ������ ����
		0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
		0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
		0.0, 0.0, 1.0, 0.0, 0.0, 1.0

	};

	float LineData [3][4] = {
		-1.0f, +0.0f, 0.0f,
		+1.0f, +0.0f, 0.0f,
		+0.0f, -1.0f, 0.0f,
		+0.0f, +1.0f, 0.0f,
	};

	std::vector<glm::vec3> Vertices_Pyramid;

public:

	ShaderManager();
	~ShaderManager();
	void Init();

	//getter
	GLuint GetShaderProgramID();
	GLuint GetVAO(unsigned int num);

	//Old Functions
	void ChangePosition(float x, float y, int cnt, bool bfill);
	void RedrawTriangles(bool bFill);
	void StartTriangleMove();
	void StopTriangleMove();
	void TriangleMoveTick(bool bFill);
	//void RotateRect(bool bFill);
	void RotateRect(int RectNum, const float Angle);

	//Initiator
private:
	void MakeVertexShaders();
	void MakeFragmentShaders();
	GLuint MakeShaderProgram();
	//Old - 2D Triangles
	GLvoid InitBuffer_Old(unsigned const int num);
	//New - 3D
	GLvoid InitCamera();
	GLvoid InitLocations();
	//New,3D but tutorial used
	GLvoid InitMVPs();
	//Debug
	void PrintDebug(const char* Message);

	//InitBuffers
public:
	GLvoid InitBuffer_Under();
	GLvoid InitBuffer_New();
	GLvoid InitBuffer_Pyramid();
	GLvoid InitBuffer_Line();

	//Called at Main::DrawScene
	GLvoid DrawScene_Cube();
	GLvoid DrawScene_Line();
	GLvoid DrawScene_Pyramid();
	GLvoid SetDepth(); //NOT USED
	GLvoid DrawScene_Num12();

	//Change Options
	GLvoid ToggleDepth();
	GLvoid TogglePolygonMode();
	GLvoid SetXMode(RotationMode mode);
	GLvoid SetYMode(RotationMode mode);
	GLvoid TogglePyramid();

	//Reset
	GLvoid ResetModel();
	GLvoid DeResetModel();
	GLvoid UnRotateTemp();
	GLvoid DeUnRotateTemp();

	//static Trans
	GLvoid MoveModel(GLfloat X, GLfloat Y,GLfloat Z, bool ShouldDraw);
	GLvoid RotateModel(GLfloat X, GLfloat Y, GLfloat Z);
};
