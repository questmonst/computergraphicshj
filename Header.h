#pragma once
#include <iostream>

#include <gl/glew.h>
#include <gl/freeglut.h>

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <cmath>
#include <vector>

#define WINDOW_SIZE_WIDTH 500
#define WINDOW_SIZE_HEIGHT 500
#define NormalAngle 45.f
static bool bFill = true;

enum RotationMode
{
	MinusRotate,
	None,
	PlusRotate
};

static GLfloat Under[] =
{
	-10.0f, -0.2f, +10.0f,
	-10.0f, -0.2f, -10.0f,
	+10.0f, -0.2f, -10.0f,
	+10.0f, -0.2f, +10.0f,
};

static unsigned int UnderIndex[] =
{
	0, 1, 2,
	2, 3, 0
};

static GLfloat g_vertex_buffer_data[] = {
	//6면체->12개삼각형->36개의 점
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,

	1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,

	1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	1.0f, 1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f,

	1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,

	-1.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,

	1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,

	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f
};

static GLfloat pyramid_vertex_buffer_data[]{
	//5면체,6개의 삼각형-> 18개의 점
	1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	1.0f, -1.0f, -1.0f,
	0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 1.0f,

	1.0f, -1.0f, 1.0f,
	0.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, 1.0f,

	-1.0f, -1.0f, 1.0f,
	0.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, -1.0f,

	0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,

	1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f
};



static GLfloat g_color_buffer_data[] = {

	1.f, 0.f, 0.f,
	1.f, 0.f, 0.f,
	1.f, 0.f, 0.f, //red

	1.f, 0.f, 0.f,
	1.f, 0.f, 0.f,
	1.f, 0.f, 0.f, //red

	0.f, 1.f, 0.f,
	0.f, 1.f, 0.f,
	0.f, 1.f, 0.f, // green

	0.f, 1.f, 0.f,
	0.f, 1.f, 0.f,
	0.f, 1.f, 0.f, //green

	0.f, 0.f, 1.0f,
	0.f, 0.f, 1.0f,
	0.f, 0.f, 1.0f, //blue

	
	0.f, 0.f, 1.0f,
	0.f, 0.f, 1.0f,
	0.f, 0.f, 1.0f, //blue

	1.f, 1.f, 0.f,
	1.f, 1.f, 0.f,
	1.f, 1.f, 0.f, //skyblue

	1.f, 1.f, 0.f,
	1.f, 1.f, 0.f,
	1.f, 1.f, 0.f, //skyblue

	0.f, 1.f, 1.f,
	0.f, 1.f, 1.f,
	0.f, 1.f, 1.f, //yellow

	0.f, 1.f, 1.f,
	0.f, 1.f, 1.f,
	0.f, 1.f, 1.f, //yellow

	1.f, 0.f, 1.f,
	1.f, 0.f, 1.f,
	1.f, 0.f, 1.f, //pupple
		 
	1.f, 0.f, 1.f,
	1.f, 0.f, 1.f,
	1.f, 0.f, 1.f //pupple



};

static bool LoadObj(
	const char* path,
	std::vector<glm::vec3>& out_vertices,
	std::vector<glm::vec2>& out_uvs,
	std::vector<glm::vec3>& out_normals
)
{
	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;

	FILE* file = fopen(path, "r");
	if (file == NULL)
	{
		printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
		getchar();
		return false;
	}

	while (1)
	{
		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0)
		{
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y;
			// Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0)
		{
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0],
			                     &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2],
			                     &normalIndex[2]);
			if (matches != 9)
			{
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				fclose(file);
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else
		{
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}
	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertexIndices.size(); i++)
	{
		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);
	}
	fclose(file);
	return true;
}

static char* FileToBuf(const char* TargetFile)
{
	FILE* fptr;
	long length;
	char* buf;
	fptr = fopen(TargetFile, "rb"); // Open file for reading 
	if (!fptr) // Return NULL on failure
		return NULL;
	fseek(fptr, 0, SEEK_END); // Seek to the end of the file 
	length = ftell(fptr); // Find out how many bytes into the file we are
	buf = (char*)malloc(length + 1); // Allocate a buffer for the entire length of the file and a null terminator 
	fseek(fptr, 0, SEEK_SET); // Go back to the beginning of the file 
	fread(buf, length, 1, fptr); // Read the contents of the file in to the buffer 
	fclose(fptr); // Close the file 
	buf[length] = 0; // Null terminator 

	return buf; // Return the buffer 
}

static float WindowFloatToGlutWidth(float Windowflt)
{
	return (Windowflt / WINDOW_SIZE_WIDTH) * 2 - 1.0f;
}

static float WindowFloatToGlutHeight(float Windowflt)
{
	return -1.0f * ((Windowflt / WINDOW_SIZE_HEIGHT) * 2 - 1.0f);
}

static float GlutHeightToFloat(float glutf)
{
	return -glutf + 1.0f * WINDOW_SIZE_HEIGHT / 2; //?
}

static float GlutWidthToFloat(float glutf)
{
	return glutf * +1.0f * WINDOW_SIZE_WIDTH / 2; //?
}

static void Rotate(float& x, float& y, float angle)
{
	float rad = angle * (3.14159265358979f / 180.f);
	float TempX = cos(rad) * x - sin(rad) * y;
	float TempY = sin(rad) * x + cos(rad) * y;

	x = TempX;
	y = TempY;
}
