#include <iostream>

#include "ShaderManager.h"
#include "Triangle.h"

ShaderManager::ShaderManager()
{
	Triangles[0] = new Triangle
	{
		-0.2, -0.1
	};
	Triangles[0]->ChangeColor
	(1.0, 0.0, 0.0,
	 1.0, 0.0, 0.0,
	 1.0, 0.0, 0.0
	);
	Triangles[1] = new Triangle
	{
		0.0, -0.1
	};
	Triangles[1]->ChangeColor
	(0.0, 1.0, 0.0,
	 0.0, 1.0, 0.0,
	 0.0, 1.0, 0.0
	);
	Triangles[2] = new Triangle
	{
		0.0, 0.1
	};
	Triangles[2]->ChangeColor
	(0.5, 0.0, 1.0,
	 0.5, 0.0, 1.0,
	 0.5, 0.0, 1.0
	);
	Triangles[3] = new Triangle
	{
		-0.2, 0.1
	};
	Triangles[3]->ChangeColor
	(
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0
	);

	bUseDepth = true;
	XMode = None;
	YMode = None;
	bFillPolies = true;
	bUseCube = true;
}

ShaderManager::~ShaderManager()
{
	delete[] Triangles;
}

void ShaderManager::Init()
{
	VertexShaderSource = FileToBuf("VertexShader.glsl");
	FragmentShaderSource = FileToBuf("FragmentShader.glsl");

	MakeVertexShaders();
	MakeFragmentShaders();
	ShaderProgramID = MakeShaderProgram();

	//InitCamera();
	InitLocations();
	InitMVPs();

	//InitBuffer_Old(5);
	//InitBuffer_Under();

	InitBuffer_New();
	InitBuffer_Line();
	InitBuffer_Pyramid();

	RotateModel(+1.0, -0.0, 0.f);
	RotateModel(0.0, -1.0, 0.f);
	//RotateModel(0.f, Defa0ultRotationY, 0.f);
}

GLvoid ShaderManager::InitCamera()
{
	CameraX = 0.0f;
	CameraZ = 0.0f;
	CameraY = 0.0f;
	cameraPos = glm::vec3(0.0f + CameraX + CameraY, 0.0f, 8.0f + CameraZ);
	cameraDirection = glm::vec3(0.0f + CameraX, 1.0f, 0.0f);
	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
}

GLvoid ShaderManager::InitLocations()
{
	ModelLocation = glGetUniformLocation(ShaderProgramID, "Model");
	ViewLocation = glGetUniformLocation(ShaderProgramID, "View");
	ProjectionLocation = glGetUniformLocation(ShaderProgramID, "Projection");

	LocationX = 0.f;
	LocationY = 0.f;
	LocationZ = 0.f;
}

GLvoid ShaderManager::InitMVPs()
{
	MatrixID = glGetUniformLocation(ShaderProgramID, "MVP");

	Projection = glm::perspective(glm::radians(30.0f), (float)WINDOW_SIZE_WIDTH / (float)WINDOW_SIZE_HEIGHT, 0.1f,
	                              200.0f);
	View = glm::lookAt(glm::vec3(0, 0, +15), // Camera is at (4,3,-3), in World Space
	                   glm::vec3(0, 0, 0), // and looks at the origin
	                   glm::vec3(0, 1, 0) // Head is up (set to 0,-1,0 to look upside-down)
	);
	Model = glm::mat4(1.0f);
	MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around
}

void ShaderManager::PrintDebug(const char* Message)
{
	std::cout << Message << std::endl;
}

GLuint ShaderManager::GetShaderProgramID()
{
	return ShaderProgramID;
}

GLuint ShaderManager::GetVAO(unsigned int num)
{
	return VAO[num];
}

void ShaderManager::ChangePosition(float x, float y, int cnt, bool bfill)
{
	Triangles[cnt]->ChangePosition(x, y);

	if (bfill)
	{
		glBindVertexArray(VAO[cnt]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[cnt * 2]);
		glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), Triangles[cnt]->TrianglePoints, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
		glEnableVertexAttribArray(0);
	}
	else
	{
		glBindVertexArray(VAO[cnt]); //--- VAO를 바인드하기
		glBindBuffer(GL_ARRAY_BUFFER, VBO[cnt * 2]); //0
		glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), Triangles[cnt]->TriangleLines, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
		glEnableVertexAttribArray(0);
	}
}

void ShaderManager::MakeVertexShaders()
{
	//--- 버텍스 세이더 객체 만들기
	VertexShader = glCreateShader(GL_VERTEX_SHADER);
	//--- 세이더 코드를 세이더 객체에 넣기
	glShaderSource(VertexShader, 1, (const GLchar**)&VertexShaderSource, 0);
	//--- 버텍스 세이더 컴파일하기
	glCompileShader(VertexShader);
	//--- 컴파일이 제대로 되지 않은 경우: 에러 체크

	GLint result;
	GLchar errorLog[512];

	glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(VertexShader, 512, NULL, errorLog);
		std::cerr << "ERROR:vertex shader 컴파일 실패\n" << errorLog << std::endl;
		return;
	}
}

void ShaderManager::MakeFragmentShaders()
{
	//--- 프래그먼트 세이더 객체 만들기
	FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	//--- 세이더 코드를 세이더 객체에 넣기
	glShaderSource(FragmentShader, 1, (const GLchar**)&FragmentShaderSource, 0);
	//--- 프래그먼트 세이더 컴파일
	glCompileShader(FragmentShader);
	//--- 컴파일이 제대로 되지 않은 경우: 컴파일 에러 체크
	GLint result;
	GLchar errorLog[512];
	glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(FragmentShader, 512, NULL, errorLog);
		std::cerr << "ERROR: fragment shader 컴파일 실패\n" << errorLog << std::endl;
		return;
	}
}

GLuint ShaderManager::MakeShaderProgram()
{
	ShaderProgramID = glCreateProgram(); //--- 세이더 프로그램 만들기
	glAttachShader(ShaderProgramID, VertexShader); //--- 세이더 프로그램에 버텍스 세이더 붙이기
	glAttachShader(ShaderProgramID, FragmentShader); //--- 세이더 프로그램에 프래그먼트 세이더 붙이기

	//--- 세이더 프로그램 링크하기
	glLinkProgram(ShaderProgramID);

	GLint result;
	GLchar errorLog[512];
	glGetProgramiv(ShaderProgramID, GL_LINK_STATUS, &result); // ---세이더가 잘 연결되었는지 체크하기
	if (!result)
	{
		glGetProgramInfoLog(ShaderProgramID, 512, nullptr, errorLog);
		std::cerr << "ERROR: shader program 연결 실패\n" << errorLog << std::endl;
		return false;
	}

	//--셰이더 삭제하기
	glDeleteShader(VertexShader);
	glDeleteShader(FragmentShader);

	glUseProgram(ShaderProgramID); //--- 만들어진 세이더 프로그램 사용하기
	return ShaderProgramID;
}

GLvoid ShaderManager::InitBuffer_Old(unsigned const int VAO_Num)
{
	glGenVertexArrays(VAO_Num, VAO); //--- VAO 를 지정하고 할당하기
	glGenBuffers(VAO_Num * 2, VBO); //--- 4개의 VBO를 지정하고 할당하기

	unsigned int VBOCount = 0;

	if (bFill)
	{
		for (int VAOCount = 0; VAOCount < TriMax; ++VAOCount)
		{
			// 삼각형 좌표, 첫번째.
			glBindVertexArray(VAO[VAOCount]); //0
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]); //0
			glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), Triangles[VAOCount]->TrianglePoints, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(0);
			VBOCount++;
			//삼각형 색상, 첫번째.
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]); //1
			glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), Triangles[VAOCount]->Colors, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(1);
			VBOCount++;
		}
	}
	else
	{
		for (int VAOCount = 0; VAOCount < TriMax; ++VAOCount)
		{
			// 삼각형 좌표, 첫번째.
			glBindVertexArray(VAO[VAOCount]); //--- VAO를 바인드하기
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]); //0
			glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), Triangles[VAOCount]->TriangleLines, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(0);
			VBOCount++;
			//삼각형 색상, 첫번째.
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]);
			glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), Triangles[VAOCount]->Colors, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(1);
			VBOCount++;
		}
	}

	//---라인데이터: 좌표계를 그리기 위한 VAO
	glBindVertexArray(VAO[4]);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[8]);
	glBufferData(GL_ARRAY_BUFFER, sizeof (LineData), LineData, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
	glEnableVertexAttribArray(1);

	std::cout << "InitBuffer_Old Successed" << std::endl;
}

GLvoid ShaderManager::InitBuffer_Under()
{
	glBindVertexArray(UnderVAO);
	glGenVertexArrays(1, &UnderVAO);
	GLuint PyramidVBO[2], PyramidEBO;
	glGenBuffers(2, PyramidVBO); // 2개의 VBO지정하고 할당하기
	glGenBuffers(1, &PyramidEBO);
	glBindBuffer(GL_ARRAY_BUFFER, PyramidVBO[0]);

	glBufferData(GL_ARRAY_BUFFER, sizeof(UnderVAO), Under, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, PyramidEBO); // GL_ELEMENT_ARRAY_BUFFER 버퍼유형으로바인딩 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(UnderIndex), UnderIndex, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, PyramidVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(UnderIndex), UnderIndex, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);
}

GLvoid ShaderManager::InitBuffer_New()
{
	//vertex buffer
	glGenBuffers(1, &VertexBuffer_Cube);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer_Cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	//color buffer
	glGenBuffers(1, &ColorBuffer_Cube);
	glBindBuffer(GL_ARRAY_BUFFER, ColorBuffer_Cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
}

GLvoid ShaderManager::InitBuffer_Pyramid()
{
	//LoadObj()

	//vertex buffer
	glGenBuffers(1, &VertexBuffer_Pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer_Pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_vertex_buffer_data), pyramid_vertex_buffer_data, GL_STATIC_DRAW);

	//color buffer
	glGenBuffers(1, &ColorBuffer_Pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, ColorBuffer_Pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_vertex_buffer_data), pyramid_vertex_buffer_data, GL_STATIC_DRAW);
}

GLvoid ShaderManager::InitBuffer_Line()
{
	glBindVertexArray(VAO[4]);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[8]);
	glBufferData(GL_ARRAY_BUFFER, sizeof (LineData), LineData, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	//Color_Black
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
	glEnableVertexAttribArray(1);
}

GLvoid ShaderManager::DrawScene_Cube()
{
	if (bFillPolies)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	//std::cout << "ShaderManager::DrawScene_Cube, Called" << std::endl;
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	if (bUseDepth)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);
	}

	glUseProgram(ShaderProgramID);

	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer_Cube);
	glVertexAttribPointer(
		0, // attribute. No particular reason for 0, but must match the layout in the shader.
		3, // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0, // stride
		(void*)0 // array buffer offset
	);

	// 2nd attribute buffer : colors
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, ColorBuffer_Cube);
	glVertexAttribPointer(
		1, // attribute. No particular reason for 1, but must match the layout in the shader.
		3, // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0, // stride
		(void*)0 // array buffer offset
	);

	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3); // 12*3 indices starting at 0 -> 12 triangles

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	glutSwapBuffers();
}

GLvoid ShaderManager::DrawScene_Line()
{
	//std::cout << "ShaderManager::DrawScene_Line, Called" << std::endl;

	glBindVertexArray(GetVAO(4));

	glDrawArrays(GL_LINES, 0, 4);
}

GLvoid ShaderManager::DrawScene_Pyramid()
{
	if (bFillPolies)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	//std::cout << "ShaderManager::DrawScene_Cube, Called" << std::endl;
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	if (bUseDepth)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);
	}

	glUseProgram(ShaderProgramID);

	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer_Pyramid);
	glVertexAttribPointer(
		0, // attribute. No particular reason for 0, but must match the layout in the shader.
		3, // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0, // stride
		(void*)0 // array buffer offset
	);

	// 2nd attribute buffer : colors
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, ColorBuffer_Pyramid);
	glVertexAttribPointer(
		1, // attribute. No particular reason for 1, but must match the layout in the shader.
		3, // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0, // stride
		(void*)0 // array buffer offset
	);

	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, 6 * 3); // 12*3 indices starting at 0 -> 12 triangles

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	glutSwapBuffers();
}

GLvoid ShaderManager::SetDepth()
{
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one

	glDepthFunc(GL_LESS);
}

GLvoid ShaderManager::DrawScene_Num12()
{
	glUseProgram(ShaderProgramID); //redundant?

	//회전행렬 생성

	glm::vec3 RotationMatrix;
	glm::vec3 RotationMatrix2;

	switch (static_cast<int>(XMode))
	{
	case 0: //MinusRotate
		RotationMatrix = glm::vec3(-1.f, +0.f, +0.f);
		break;
	case 1: //None
		RotationMatrix = glm::vec3(+0.f, +0.f, +0.f);
		break;
	case 2: //PlusRotate
		RotationMatrix = glm::vec3(+1.f, +0.f, +0.f);
		break;
	default:
		RotationMatrix = glm::vec3(+0.f, +0.f, +0.f);

		break;
	}

	switch (static_cast<int>(YMode))
	{
	case 0: //MinusRotate
		RotationMatrix2 = glm::vec3(+0.f, -1.f, +0.f);
		break;
	case 1: //None
		RotationMatrix2 = glm::vec3(+0.f, +0.f, +0.f);
		break;
	case 2: //PlusRotate
		RotationMatrix2 = glm::vec3(+0.f, +1.f, +0.f);

		break;
	default:
		RotationMatrix2 = glm::vec3(+0.f, +0.f, +0.f);
		break;
	}

	RotationMatrix = RotationMatrix + RotationMatrix2;

	//회전이 0 이아니면 회전
	//--- model 행렬에 이동 변환 적용
	if (RotationMatrix != glm::vec3(0.f, 0.f, 0.f))
	{
		Model = glm::rotate(Model, 5.f, RotationMatrix);
		//ModelLocation을 MVP에 적용
		ModelLocation = glGetUniformLocation(ShaderProgramID, "MVP");
		//std::cout << "ShaderManager::ModelLocation :" << ModelLocation << std::endl;

		MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around
	}

	if (bUseCube)
	{
		DrawScene_Cube();
	}
	else
	{
		DrawScene_Pyramid();
	}
}

GLvoid ShaderManager::MoveModel(GLfloat X, GLfloat Y, GLfloat Z, bool ShouldDraw)
{
	//needs quaternion?
	//UnRotateTemp();

	if (ShouldDraw)
	{
		LocationX += X;
		LocationY += Y;
		LocationZ += Z;
	}

	glm::vec3 MoveMatrix(X, Y, Z);

	Model = glm::translate(Model, MoveMatrix);
	ModelLocation = glGetUniformLocation(ShaderProgramID, "MVP");
	MVP = Projection * View * Model;

	//if(!ShouldDraw)
	//{
	//	return;
	//}

	if (bUseCube)
	{
		DrawScene_Cube();
	}
	else
	{
		DrawScene_Pyramid();
	}
}

GLvoid ShaderManager::RotateModel(GLfloat X, GLfloat Y, GLfloat Z) //temply redundant
{
	RotationX += X;
	RotationY += Y;
	RotationZ += Z;

	GLfloat TempLocationX = LocationX;
	GLfloat TempLocationY = LocationY;
	GLfloat TempLocationZ = LocationZ;

	std::cout << "RotationModel:: X :" << RotationX << "Y : " << RotationY << "Z :" << RotationZ << std::endl;

	const glm::vec3 StaticRotationMatrix(+X, +Y, +Z);
	glPushMatrix();
	Model = glm::rotate(Model, DefalutAngle, StaticRotationMatrix);
	//ModelLocation을 MVP에 적용
	ModelLocation = glGetUniformLocation(ShaderProgramID, "MVP");

	MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around

	glPopMatrix();
	
	if (bUseCube)
	{
		DrawScene_Cube();
	}
	else
	{
		DrawScene_Pyramid();
	}
}

GLvoid ShaderManager::UnRotateTemp()
{
	const glm::vec3 StaticRotationMatrix(-RotationX, -RotationY, -RotationZ);
	std::cout << "UnRotate, X :" << -RotationX << "Y : " << - RotationY << "Z :" << -RotationZ << std::endl;

	if (StaticRotationMatrix != glm::vec3(0.f, 0.f, 0.f))
	{
		Model = glm::rotate(Model, DefalutAngle, StaticRotationMatrix);
		//ModelLocation을 MVP에 적용
		ModelLocation = glGetUniformLocation(ShaderProgramID, "MVP");
		//std::cout << "ShaderManager::ModelLocation :" << ModelLocation << std::endl;

		MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around
	}

	if (bUseCube)
	{
		DrawScene_Cube();
	}
	else
	{
		DrawScene_Pyramid();
	}
}

GLvoid ShaderManager::DeUnRotateTemp()
{
	const glm::vec3 StaticRotationMatrix(RotationX, RotationX, RotationZ);
	std::cout << "DeUnRotate, X :" << RotationX << "Y : " << RotationY << "Z :" << RotationZ << std::endl;

	if (StaticRotationMatrix != glm::vec3(0.f, 0.f, 0.f))
	{
		Model = glm::rotate(Model, DefalutAngle, StaticRotationMatrix);
		//ModelLocation을 MVP에 적용
		ModelLocation = glGetUniformLocation(ShaderProgramID, "MVP");
		//std::cout << "ShaderManager::ModelLocation :" << ModelLocation << std::endl;

		MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around
	}
	if (bUseCube)
	{
		DrawScene_Cube();
	}
	else
	{
		DrawScene_Pyramid();
	}
}

GLvoid ShaderManager::ToggleDepth()
{
	PrintDebug("ToggleDepth Called");
	bUseDepth = bUseDepth ? false : true;
}

GLvoid ShaderManager::TogglePolygonMode()
{
	PrintDebug("TogglePolygonMode Called");
	bFillPolies = bFillPolies ? false : true;
}

GLvoid ShaderManager::TogglePyramid()
{
	PrintDebug("TogglePyramidMode Called");
	bUseCube = bUseCube ? false : true;
}

GLvoid ShaderManager::SetXMode(RotationMode mode)
{
	PrintDebug("SetXMode Called");
	std::cout << "XMode :" << XMode << std::endl;

	XMode = mode;
}

GLvoid ShaderManager::SetYMode(RotationMode mode)
{
	PrintDebug("SetYMode Called");
	YMode = mode;
}

GLvoid ShaderManager::ResetModel()
{
	TempModel = Model;
	Model = glm::mat4(1.0f);
	MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around
}

GLvoid ShaderManager::DeResetModel()
{
	Model = -TempModel;
	MVP = Projection * View * Model;
}

void ShaderManager::RedrawTriangles(bool bFill)
{
	//inbuffer초기화넣어야할수도
	unsigned int VBOCount = 0;
	if (bFill)
	{
		for (int VAOCount = 0; VAOCount < TriMax; ++VAOCount)
		{
			// 삼각형 좌표, 첫번째.
			glBindVertexArray(VAO[VAOCount]); //0
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]); //0
			glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), Triangles[VAOCount]->TrianglePoints, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(0);
			VBOCount++;
			//삼각형 색상, 첫번째.
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]); //1
			glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), Triangles[VAOCount]->Colors, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(1);
			VBOCount++;
		}
	}
	else
	{
		for (int VAOCount = 0; VAOCount < TriMax; ++VAOCount)
		{
			// 삼각형 좌표, 첫번째.
			glBindVertexArray(VAO[VAOCount]); //--- VAO를 바인드하기
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]); //0
			glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), Triangles[VAOCount]->TriangleLines, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(0);
			VBOCount++;
			//삼각형 색상, 첫번째.
			glBindBuffer(GL_ARRAY_BUFFER, VBO[VBOCount]);
			glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), Triangles[VAOCount]->Colors, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
			glEnableVertexAttribArray(1);
		}
	}
}

void ShaderManager::StartTriangleMove()
{
	for (int i = 0; i < 4; ++i)
	{
		Triangles[i]->MoveStart();
	}
}

void ShaderManager::StopTriangleMove()
{
	for (int i = 0; i < 4; ++i)
	{
		Triangles[i]->MoveStop();
	}
}

void ShaderManager::TriangleMoveTick(bool bFill)
{
	for (int i = 0; i < 4; ++i)
	{
		Triangles[i]->UpdateMove();
		Triangles[i]->UpdateRotation();
	}
	RedrawTriangles(bFill);
}

void ShaderManager::RotateRect(int RectNum, const float Angle)
{
	Triangle* TargetTriangle = Triangles[RectNum];
	float TempX, TempY;

	// 원 위치 저장용

	TargetTriangle->GetPosition(TempX, TempY);
	std::cout << "TempX :" << TempX << std::endl;
	std::cout << "TempY :" << TempY << std::endl;

	TargetTriangle->MovePosition(-TempX, -TempY);

	Rotate(TargetTriangle->TrianglePoints[0][0], TargetTriangle->TrianglePoints[0][1], Angle);
	Rotate(TargetTriangle->TrianglePoints[1][0], TargetTriangle->TrianglePoints[1][1], Angle);
	Rotate(TargetTriangle->TrianglePoints[2][0], TargetTriangle->TrianglePoints[2][1], Angle);

	TargetTriangle->MovePosition(TempX, TempY);

	RedrawTriangles(true);
}
