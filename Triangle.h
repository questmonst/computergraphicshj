#pragma once
#include <gl/glew.h>
#include "Header.h"

class Triangle
{
public:
	GLfloat TrianglePoints[3][3];
	GLfloat TriangleLines[3][6];
	GLfloat Colors[3][3];

	const GLfloat Height = 0.2f;
	const GLfloat Width = 0.1f;
	GLfloat FrontAngle = 0.f;

	Triangle(GLfloat ax, GLfloat ay, GLfloat az,
	         GLfloat bx, GLfloat by, GLfloat bz,
	         GLfloat cx, GLfloat cy, GLfloat cz
	);
	Triangle(GLfloat x, GLfloat y);

	void ChangePosition(
		GLfloat ax, GLfloat ay, GLfloat az,
		GLfloat bx, GLfloat by, GLfloat bz,
		GLfloat cx, GLfloat cy, GLfloat cz
	);

	void ChangeColor(
		GLfloat ax, GLfloat ay, GLfloat az,
		GLfloat bx, GLfloat by, GLfloat bz,
		GLfloat cx, GLfloat cy, GLfloat cz
	);
	void ChangePosition(GLfloat x, GLfloat y);
	void MovePosition(GLfloat x, GLfloat y);
	void GetPosition(GLfloat& x, GLfloat& y);
	void PrintPosition();
	void UpdateMove();
	void UpdateRotation();
	void MoveStart();
	void MoveStop();
	void RotateSelf(GLfloat Angle,bool bShouldUpdate);
	void FixRotate(GLfloat Angle);
	GLfloat CalcAngle();


	float XMove{0.f};
	float YMove{0.f};
};
