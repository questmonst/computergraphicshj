#version 330 core

// 이 셰이더 실행마다 다른 데이터
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexColor;

// 출력 데이터 ; 각 프레그먼트로 인터폴레이트
out vec3 fragmentColor;
// 모든 메쉬가 움직이지 않음
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;
uniform mat4 MVP;

void main(){	

	// 버텍스 위치 출력값 , 클립 공간에서: MVP * position
	gl_Position =  MVP * vec4(vertexPosition_modelspace,1);

	// 모든 컬러는 각 프레그먼트로 인터폴레이트됨
	fragmentColor = vertexColor;
}