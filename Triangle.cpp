#include "Triangle.h"

#include <iostream>
#include <random>
#include <chrono>

#include "Header.h"

Triangle::Triangle(GLfloat ax, GLfloat ay, GLfloat az, GLfloat bx, GLfloat by, GLfloat bz, GLfloat cx, GLfloat cy,
                   GLfloat cz)
{
	std::cout << "Triangle Init, Initer" << std::endl;

	TrianglePoints[0][0] = ax;
	TrianglePoints[0][1] = ay;
	TrianglePoints[0][2] = az;
	TrianglePoints[1][0] = bx;
	TrianglePoints[1][1] = by;
	TrianglePoints[1][2] = bz;
	TrianglePoints[2][0] = cx;
	TrianglePoints[2][1] = cy;
	TrianglePoints[2][2] = cz;

	TriangleLines[0][0] = ax;
	TriangleLines[0][1] = ay;
	TriangleLines[0][2] = az;

	TriangleLines[0][3] = bx;
	TriangleLines[0][4] = by;
	TriangleLines[0][5] = bz;

	TriangleLines[1][0] = bx;
	TriangleLines[1][1] = by;
	TriangleLines[1][2] = bz;

	TriangleLines[1][3] = cx;
	TriangleLines[1][4] = cy;
	TriangleLines[1][5] = cz;

	TriangleLines[2][0] = cx;
	TriangleLines[2][1] = cy;
	TriangleLines[2][2] = cz;

	TriangleLines[2][3] = ax;
	TriangleLines[2][4] = ay;
	TriangleLines[2][5] = az;

	FrontAngle = 0.f;
}

Triangle::Triangle(GLfloat x, GLfloat y)
{
	TrianglePoints[0][0] = x;
	TrianglePoints[1][0] = x + Width; //b
	TrianglePoints[2][0] = x + Width / 2; //c

	TrianglePoints[0][1] = y;
	TrianglePoints[1][1] = y;
	TrianglePoints[2][1] = y + Height;

	TriangleLines[0][0] = x;
	TriangleLines[0][1] = y;
	TriangleLines[0][2] = 0.f;

	TriangleLines[0][3] = x + Height;
	TriangleLines[0][4] = y;
	TriangleLines[0][5] = 0.f;

	TriangleLines[1][0] = x + Height;
	TriangleLines[1][1] = y;
	TriangleLines[1][2] = 0.f;

	TriangleLines[1][3] = x + Width;
	TriangleLines[1][4] = y + Height;
	TriangleLines[1][5] = 0.f;

	TriangleLines[2][0] = x + Width;
	TriangleLines[2][1] = y + Height;
	TriangleLines[2][2] = 0.f;

	TriangleLines[2][3] = x;
	TriangleLines[2][4] = y;
	TriangleLines[2][5] = 0.f;
}

void Triangle::ChangePosition(GLfloat ax, GLfloat ay, GLfloat az, GLfloat bx, GLfloat by, GLfloat bz, GLfloat cx,
                              GLfloat cy, GLfloat cz)
{
	TrianglePoints[0][0] = ax;
	TrianglePoints[0][1] = ay;
	TrianglePoints[0][2] = az;
	TrianglePoints[1][0] = bx;
	TrianglePoints[1][1] = by;
	TrianglePoints[1][2] = bz;
	TrianglePoints[2][0] = cx;
	TrianglePoints[2][1] = cy;
	TrianglePoints[2][2] = cz;

	TriangleLines[0][0] = ax;
	TriangleLines[0][1] = ay;
	TriangleLines[0][2] = az;

	TriangleLines[0][3] = bx;
	TriangleLines[0][4] = by;
	TriangleLines[0][5] = bz;

	TriangleLines[1][0] = bx;
	TriangleLines[1][1] = by;
	TriangleLines[1][2] = bz;

	TriangleLines[1][3] = cx;
	TriangleLines[1][4] = cy;
	TriangleLines[1][5] = cz;

	TriangleLines[2][0] = cx;
	TriangleLines[2][1] = cy;
	TriangleLines[2][2] = cz;

	TriangleLines[2][3] = ax;
	TriangleLines[2][4] = ay;
	TriangleLines[2][5] = az;
}

void Triangle::ChangeColor(GLfloat ax, GLfloat ay, GLfloat az, GLfloat bx, GLfloat by, GLfloat bz, GLfloat cx,
                           GLfloat cy, GLfloat cz)
{
	Colors[0][0] = ax;
	Colors[0][1] = ay;
	Colors[0][2] = az;

	Colors[1][0] = bx;
	Colors[1][1] = by;
	Colors[1][2] = bz;

	Colors[2][0] = cx;
	Colors[2][1] = cy;
	Colors[2][2] = cz;
}

void Triangle::UpdateMove()
{
	GLfloat tempX = TrianglePoints[0][0];
	GLfloat tempY = TrianglePoints[0][1];

	if (tempX + XMove > 0.8f)
	{
		XMove = -0.01f;
	}
	if (tempX + XMove < -0.8f)
	{
		XMove = 0.01f;
	}

	if (tempY + YMove > 0.8f)
	{
		YMove = -0.01f;
	}
	if (tempY + YMove < -0.8f)
	{
		YMove = +0.01f;
	}

	//RotateSelf(10.f,true);
	//FixRotate(10);//임시

	ChangePosition(tempX + XMove, tempY + YMove);
}

void Triangle::UpdateRotation()
{
	if (XMove > 0)
	{
		if (YMove > 0) { RotateSelf(-NormalAngle, true); }
		if (YMove < 0) { RotateSelf(NormalAngle * 5, true); }
	}
	if (XMove < 0)
	{
		if (YMove > 0) { RotateSelf(-NormalAngle * 7, true); } //좌상단
		if (YMove < 0) { RotateSelf(NormalAngle * 3, true); }
	}
}

void Triangle::MoveStart()
{
	std::random_device rd;

	// random_device 를 통해 난수 생성 엔진을 초기화 한다.
	std::mt19937 gen(rd());

	// 0 부터 99 까지 균등하게 나타나는 난수열을 생성하기 위해 균등 분포 정의.
	std::uniform_int_distribution<int> dis(0, 3);
	std::uniform_int_distribution<int> dis2(0, 3);

	switch (dis(rd))
	{
	case 0:

		XMove = 0.01f;

		break;

	case 1:

		XMove = -0.01f;

		break;

	default:
		XMove = 0.01f;
		YMove = 0.01f;
		break;
	}

	switch (dis2(rd))
	{
	case 0:

		YMove = 0.01f;
		break;

	case 1:

		YMove = -0.01f;
		break;

	default:
		XMove = 0.01f;
		YMove = 0.01f;
		break;
	}
}

void Triangle::MoveStop()
{
	XMove = 0.f;
	YMove = 0.f;
}

void Triangle::RotateSelf(GLfloat Angle, bool bShouldUpdate)
{
	float TempX, TempY;

	// 원 위치 저장용
	GetPosition(TempX, TempY);

	MovePosition(-TempX, -TempY);

	Rotate(TrianglePoints[0][0], TrianglePoints[0][1], Angle);
	Rotate(TrianglePoints[1][0], TrianglePoints[1][1], Angle);
	Rotate(TrianglePoints[2][0], TrianglePoints[2][1], Angle);

	MovePosition(TempX, TempY);
}

void Triangle::FixRotate(GLfloat Angle)
{
	float TempX, TempY;

	GetPosition(TempX, TempY);

	MovePosition(-TempX, -TempY);
	std::cout << "Fix Rotated" << std::endl;

	Rotate(TrianglePoints[0][0], TrianglePoints[0][1], -Angle);
	Rotate(TrianglePoints[1][0], TrianglePoints[1][1], -Angle);
	Rotate(TrianglePoints[2][0], TrianglePoints[2][1], -Angle);

	MovePosition(TempX, TempY);

	FrontAngle = Angle;

	RotateSelf(Angle, false);
}

GLfloat Triangle::CalcAngle()
{
	float vector = sqrt(pow(XMove, 2) + pow(YMove, 2));

	std::cout << "vector" << vector << std::endl;;

	float angle = 0.f;

	if (XMove > 0 || 0 < XMove)
	{
		angle = acos(vector / XMove);
	}
	std::cout << "Xmove :" << XMove << std::endl;;
	std::cout << "Angle :" << angle << std::endl;;

	return angle;
}

void Triangle::GetPosition(GLfloat& x, GLfloat& y)
{
	x = TrianglePoints[0][0];
	y = TrianglePoints[0][1];
}

void Triangle::PrintPosition()
{
}

void Triangle::ChangePosition(GLfloat x, GLfloat y)
{
	TrianglePoints[0][0] = x;
	TrianglePoints[1][0] = x + Width; //b
	TrianglePoints[2][0] = x + Width / 2; //c

	TrianglePoints[0][1] = y;
	TrianglePoints[1][1] = y;
	TrianglePoints[2][1] = y + Height;

	TriangleLines[0][0] = x;
	TriangleLines[0][1] = y;
	TriangleLines[0][2] = 0.f;

	TriangleLines[0][3] = x + Height;
	TriangleLines[0][4] = y;
	TriangleLines[0][5] = 0.f;

	TriangleLines[1][0] = x + Height;
	TriangleLines[1][1] = y;
	TriangleLines[1][2] = 0.f;

	TriangleLines[1][3] = x + Width;
	TriangleLines[1][4] = y + Height;
	TriangleLines[1][5] = 0.f;

	TriangleLines[2][0] = x + Width;
	TriangleLines[2][1] = y + Height;
	TriangleLines[2][2] = 0.f;

	TriangleLines[2][3] = x;
	TriangleLines[2][4] = y;
	TriangleLines[2][5] = 0.f;
}

void Triangle::MovePosition(const GLfloat x, const GLfloat y)
{
	//std::cout << "MovePosition:: x: " << x << " , y" << y << std::endl;

	TrianglePoints[0][0] += x;
	TrianglePoints[1][0] += x;
	TrianglePoints[2][0] += x;

	TrianglePoints[0][1] += y;
	TrianglePoints[1][1] += y;
	TrianglePoints[2][1] += y;

	TriangleLines[0][0] += x;
	TriangleLines[0][1] += y;

	TriangleLines[0][3] += x;
	TriangleLines[0][4] += y;

	TriangleLines[1][0] += x;
	TriangleLines[1][1] += y;

	TriangleLines[1][3] += x;
	TriangleLines[1][4] += y;

	TriangleLines[2][0] += x;
	TriangleLines[2][1] += y;

	TriangleLines[2][3] += x;
	TriangleLines[2][4] += y;
}
