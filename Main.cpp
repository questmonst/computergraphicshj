﻿#pragma once
#include "Header.h"
#include "ShaderManager.h"

int countTriangles = 0;
bool bMove = false;

// Main Functions
GLvoid DrawScene();
GLvoid Reshape(int w, int h);
void Mouse(int button, int state, int x, int y);
GLvoid Keyboard(unsigned char key, int x, int y);
GLvoid SpecialKeyboard(int key, int x, int y);
void TimerFunction(int value);

ShaderManager Manager;

void main(int argc, char** argv) //--- 윈도우 출력하고 콜백함수 설정
{
	//--- 윈도우 생성하기

	glutInit(&argc, argv); // glut 초기화
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); // 디스플레이 모드 설정
	glutInitWindowPosition(100, 100); // 윈도우의 위치 지정
	glutInitWindowSize(WINDOW_SIZE_WIDTH, WINDOW_SIZE_HEIGHT); // 윈도우의 크기 지정
	glutCreateWindow("ShinHyoJae"); // 윈도우 생성 (윈도우 이름)

	//--- GLEW 초기화하기
	glewExperimental = GL_TRUE;
	glewInit();

	Manager.Init();
	//--- 세이더 읽어와서 세이더 프로그램 만들기

	glutDisplayFunc(DrawScene);
	glutReshapeFunc(Reshape);
	glutTimerFunc(100, TimerFunction, 1);
	//glutMouseFunc(Mouse); // 키보드 입력 콜백함수 지정
	glutKeyboardFunc(Keyboard); // 키보드 입력 콜백함수 지정
	glutSpecialFunc(SpecialKeyboard);

	glutMainLoop();
}

GLvoid DrawScene()
{
	//std::cout<<"DrawScene:: Called"<< std::endl;

	//Manager.DrawScene_Cube();
	//Manager.DrawScene_Line();
	//Manager.DrawScene_Num12();
	
	//if(Manager.bUseCube)
	//{
	//	Manager.DrawScene_Cube();
	//	
	//}else
	//{
	//	Manager.DrawScene_Pyramid();
	//}
}

GLvoid Reshape(int w, int h)
{
	glViewport(0, 0, w, h);
}

void Mouse(int button, int state, int x, int y)
{
	std::cout << "Mouse!";
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		std::cout << "x =  " << x << " y =  " << y << std::endl;

		std::cout << "x =  " << x << " y =  " << y << std::endl;

		float GlutX = WindowFloatToGlutWidth(x);
		float GlutY = WindowFloatToGlutHeight(y);

		std::cout << "glut x =  " << GlutX << std::endl;
		std::cout << "glut y =  " << GlutY << std::endl;

		Manager.ChangePosition(GlutX, GlutY, countTriangles, bFill);
		countTriangles++;
		if (countTriangles > 3)
		{
			countTriangles = 0;
		}
		glutPostRedisplay(); // 화면 재 출력

		glutTimerFunc(100, TimerFunction, 1); // 타이머함수 재 설정
	}
}

void TimerFunction(int value)
{
	glutPostRedisplay(); // 화면 재 출력

	glutTimerFunc(100, TimerFunction, 1); // 타이머함수 재 설정
}

GLvoid Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'h': //은면 적용, 해제
		Manager.ToggleDepth();
		glutPostRedisplay();
		break;
	case 'H': //은면 적용, 해제
		Manager.ToggleDepth();
		glutPostRedisplay();
		break;
	case 'x': //X축 양회전
		Manager.SetXMode(RotationMode::PlusRotate);
		Manager.RotateModel(1.f, 0.f, 0.f);
		glutPostRedisplay();
		break;
	case 'X': //X축 음회전
		Manager.SetXMode(RotationMode::MinusRotate);
		Manager.RotateModel(-1.f, 0.f, 0.f);
		glutPostRedisplay();
		break;

	case 'y': // y축 양회전
		Manager.SetYMode(RotationMode::PlusRotate);
		Manager.RotateModel(0.f, 1.f, 0.f);
		glutPostRedisplay();
		break;

	case 'Y': // y축 음회전
		Manager.SetYMode(RotationMode::MinusRotate);
		Manager.RotateModel(0.f, -1.f, 0.f);
		glutPostRedisplay();
		break;

	case 'z': // z축 양회전
		Manager.RotateModel(0.f, 0.f, 1.f);
		glutPostRedisplay();
		break;

	case 'Z': // z축 음회전
		Manager.RotateModel(0.f, 0.f, -1.f);
		glutPostRedisplay();
		break;
	case 's': // 초기 위치로 바꾸고 회전도 정지
		Manager.SetYMode(RotationMode::None);
		Manager.SetXMode(RotationMode::None);
		Manager.ResetModel();
		glutPostRedisplay();
		break;

	case 'u': // 초기 위치로 바꾸고 회전도 정지
		Manager.UnRotateTemp();
		glutPostRedisplay();
		break;

	case 'd': // 초기 위치로 바꾸고 회전도 정지
		Manager.DeUnRotateTemp();
		glutPostRedisplay();
		break;

	case 'w': // poly toggle
		Manager.TogglePolygonMode();
		glutPostRedisplay();
		break;

	case 'p': // pyramid toggle
		Manager.TogglePyramid();
		glutPostRedisplay();
		break;

		case 'P': // pyramid toggle
		Manager.DrawScene_Num12();
		glutPostRedisplay();
		break;

	case 'q':
		return;
		break;

	default:
		std::cout << key << std::endl;
		break;
	}
	glutPostRedisplay(); //--- 배경색이 바뀔때마다 출력 콜백함수를 호출하여 화면을 refresh 한다
}

GLvoid SpecialKeyboard(int key, int x, int y)
{
	using namespace std;
	if (key == GLUT_KEY_UP)
	{
		cout << "up" << endl;
		Manager.MoveModel(0.f, 1.f,0.f, true);
	}
	if (key == GLUT_KEY_DOWN)
	{
		cout << "down" << endl;
		Manager.MoveModel(0.f, -1.f,0.f,true);
	}
	if (key == GLUT_KEY_LEFT)
	{
		cout << "left" << endl;
		Manager.MoveModel(-1.f, 0.f,0.f,true);
	}
	if (key == GLUT_KEY_RIGHT)
	{
		cout << "right" << endl;
		Manager.MoveModel(1.f, 0.f,0.f,true);
	}
}
